import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import java.lang.String as String
import org.openqa.selenium.Keys as Keys

cantidadRegistros = findTestData('InformacionSimon').getRowNumbers()

for (int i = 1; i <= cantidadRegistros; i++) {
    WebUI.openBrowser('')

    WebUI.navigateToUrl('https://simon.inder.gov.co/registro')

    WebUI.maximizeWindow()

    WebUI.waitForPageLoad(5)

    WebUI.click(findTestObject('TipoPersona'))

    WebUI.focus(findTestObject('TipoPersonaSeleccionar'))

    WebUI.sendKeys(findTestObject('TipoPersonaSeleccionar'), findTestData('InformacionSimon').getValue(2, i))

    WebUI.sendKeys(findTestObject('TipoPersonaSeleccionar'), Keys.chord(Keys.ENTER))

    WebUI.scrollToElement(findTestObject('TipoDocumento'), 2)

    WebUI.click(findTestObject('TipoDocumento'))

    WebUI.focus(findTestObject('TipoDocumentoSeleccionar'))

    WebUI.sendKeys(findTestObject('TipoDocumentoSeleccionar'), findTestData('InformacionSimon').getValue(3, i))

    WebUI.sendKeys(findTestObject('TipoDocumentoSeleccionar'), Keys.chord(Keys.ENTER))

    WebUI.scrollToElement(findTestObject('NumeroDocumento'), 2)

    WebUI.sendKeys(findTestObject('NumeroDocumento'), findTestData('InformacionSimon').getValue(4, i))

    WebUI.scrollToElement(findTestObject('Nombres'), 2)

    WebUI.sendKeys(findTestObject('Nombres'), findTestData('InformacionSimon').getValue(5, i))

    WebUI.scrollToElement(findTestObject('Apellidos'), 2)

    WebUI.sendKeys(findTestObject('Apellidos'), findTestData('InformacionSimon').getValue(6, i))

    WebUI.scrollToElement(findTestObject('Sexo'), 2)

    WebUI.click(findTestObject('Sexo'))

    WebUI.focus(findTestObject('SexoSeleccionar'))

    WebUI.sendKeys(findTestObject('SexoSeleccionar'), findTestData('InformacionSimon').getValue(7, i))

    WebUI.sendKeys(findTestObject('SexoSeleccionar'), Keys.chord(Keys.ENTER))

    WebUI.scrollToElement(findTestObject('FechaNacimiento'), 2)

    WebUI.sendKeys(findTestObject('FechaNacimiento'), findTestData('InformacionSimon').getValue(8, i))

    WebUI.scrollToElement(findTestObject('ClaveUno'), 2)

    WebUI.sendKeys(findTestObject('ClaveUno'), findTestData('InformacionSimon').getValue(9, i))

    WebUI.scrollToElement(findTestObject('ClaveDos'), 2)

    WebUI.sendKeys(findTestObject('ClaveDos'), findTestData('InformacionSimon').getValue(9, i))

    WebUI.scrollToElement(findTestObject('Municipio'), 2)

    WebUI.click(findTestObject('Municipio'))

    WebUI.focus(findTestObject('MunicipioSeleccionar'))

    WebUI.sendKeys(findTestObject('MunicipioSeleccionar'), findTestData('InformacionSimon').getValue(10, i))

    WebUI.sendKeys(findTestObject('MunicipioSeleccionar'), Keys.chord(Keys.ENTER))

    WebUI.delay(5)

    if (findTestData('InformacionSimon').getValue(11, i).equals('Vereda')) {
        WebUI.click(findTestObject('DireccionUno'))

        WebUI.delay(5)

        WebUI.scrollToElement(findTestObject('Vereda'), 2)

        WebUI.click(findTestObject('Vereda'))

        WebUI.focus(findTestObject('VeredaSeleccionar'))

        WebUI.sendKeys(findTestObject('VeredaSeleccionar'), findTestData('InformacionSimon').getValue(12, i))

        WebUI.sendKeys(findTestObject('VeredaSeleccionar'), Keys.chord(Keys.ENTER))

        WebUI.scrollToElement(findTestObject('DireccionVParteUno'), 2)

        WebUI.click(findTestObject('DireccionVParteUno'))

        WebUI.focus(findTestObject('DireccionVParteUnoSeleccionar'))

        WebUI.sendKeys(findTestObject('DireccionVParteUnoSeleccionar'), findTestData('InformacionSimon').getValue(13, i))

        WebUI.sendKeys(findTestObject('DireccionVParteUnoSeleccionar'), Keys.chord(Keys.ENTER))

        WebUI.sendKeys(findTestObject('DireccionVParteDos'), findTestData('InformacionSimon').getValue(14, i))

        WebUI.sendKeys(findTestObject('DireccionVParteComplemento'), findTestData('InformacionSimon').getValue(15, i))
    } else if (findTestData('InformacionSimon').getValue(11, i).equals('Barrio')) {
        WebUI.click(findTestObject('DireccionDos'))

        WebUI.delay(5)

        WebUI.scrollToElement(findTestObject('Barrio'), 2)

        WebUI.click(findTestObject('Barrio'))

        WebUI.focus(findTestObject('BarrioSeleccionar'))

        WebUI.sendKeys(findTestObject('BarrioSeleccionar'), findTestData('InformacionSimon').getValue(12, i))

        WebUI.sendKeys(findTestObject('BarrioSeleccionar'), Keys.chord(Keys.ENTER))

        WebUI.scrollToElement(findTestObject('DireccionParteUno'), 2)

        WebUI.click(findTestObject('DireccionParteUno'))

        WebUI.focus(findTestObject('DireccionParteUnoSeleccionar'))

        WebUI.sendKeys(findTestObject('DireccionParteUnoSeleccionar'), findTestData('InformacionSimon').getValue(13, i))

        WebUI.sendKeys(findTestObject('DireccionParteUnoSeleccionar'), Keys.chord(Keys.ENTER))

        WebUI.sendKeys(findTestObject('DireccionParteDos'), findTestData('InformacionSimon').getValue(14, i))

        WebUI.click(findTestObject('DireccionParteTres'))

        WebUI.focus(findTestObject('DireccionParteTresSeleccionar'))

        WebUI.sendKeys(findTestObject('DireccionParteTresSeleccionar'), findTestData('InformacionSimon').getValue(15, i))

        WebUI.sendKeys(findTestObject('DireccionParteTresSeleccionar'), Keys.chord(Keys.ENTER))

        WebUI.click(findTestObject('DireccionParteCuatro'))

        WebUI.focus(findTestObject('DireccionParteCuatroSeleccionar'))

        WebUI.sendKeys(findTestObject('DireccionParteCuatroSeleccionar'), findTestData('InformacionSimon').getValue(16, 
                i))

        WebUI.sendKeys(findTestObject('DireccionParteCuatroSeleccionar'), Keys.chord(Keys.ENTER))

        WebUI.sendKeys(findTestObject('DireccionParteCinco'), findTestData('InformacionSimon').getValue(17, i))

        WebUI.click(findTestObject('DireccionParteSeis'))

        WebUI.focus(findTestObject('DireccionParteSeisSeleccionar'))

        WebUI.sendKeys(findTestObject('DireccionParteSeisSeleccionar'), findTestData('InformacionSimon').getValue(18, i))

        WebUI.sendKeys(findTestObject('DireccionParteSeisSeleccionar'), Keys.chord(Keys.ENTER))

        WebUI.sendKeys(findTestObject('DireccionParteSiete'), findTestData('InformacionSimon').getValue(19, i))
    }
    
    WebUI.scrollToElement(findTestObject('Estrato'), 2)

    WebUI.click(findTestObject('Estrato'))

    WebUI.focus(findTestObject('EstratoSeleccionar'))

    WebUI.sendKeys(findTestObject('EstratoSeleccionar'), findTestData('InformacionSimon').getValue(20, i))

    WebUI.sendKeys(findTestObject('EstratoSeleccionar'), Keys.chord(Keys.ENTER))

    WebUI.scrollToElement(findTestObject('CorreoElectronico'), 2)

    WebUI.sendKeys(findTestObject('CorreoElectronico'), findTestData('InformacionSimon').getValue(21, i))

    WebUI.scrollToElement(findTestObject('Telefono'), 2)

    WebUI.sendKeys(findTestObject('Telefono'), findTestData('InformacionSimon').getValue(22, i))

    WebUI.scrollToElement(findTestObject('HabeasData'), 2)

    WebUI.click(findTestObject('HabeasData'))

    WebUI.scrollToElement(findTestObject('TerminosCondiciones'), 2)

    WebUI.click(findTestObject('TerminosCondiciones'))

    WebUI.scrollToElement(findTestObject('Guardar'), 2)

    WebUI.click(findTestObject('Guardar'))

    WebUI.delay(5)

    WebUI.takeScreenshot()

    String prueba = WebUI.getText(findTestObject('Comprobacion'))

    String comparado = findTestData('InformacionSimon').getValue(23, i)

    if (prueba.contains(comparado)) {
        System.out.println('Registro exitoso en Página Simon')
    }
    
    WebUI.delay(5)

    WebUI.closeBrowser()
}

