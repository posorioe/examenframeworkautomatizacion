<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>objeto tipo de persona</description>
   <name>TipoPersona</name>
   <tag></tag>
   <elementGuidId>97051cb6-96ce-46b6-942b-acec14938c53</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = 's2id_tipopersona']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>s2id_tipopersona</value>
   </webElementProperties>
</WebElementEntity>
